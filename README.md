# OpenML dataset: Predict-Amazon-Stock-Price-tomorrow

https://www.openml.org/d/43741

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

I have created this dataset to showcase the use of predictive modeling using the stock market as a case study. This dataset is designed to help and predict tomorrow's Amazon stock price. If you want to get the most updated dataset you will need to pull them in real time. I have shared my code to pull data using Yahoo Finance API and preprocess it in Data Analytics for Fun Github Repository
The uploaded dataset is for Jan 11, 2021. 
What are the columns?
yes_changeP: Yesterday Amazon's stock price change
lastweek_changeP: Last week Amazon's stock price change
dow_yes_changeP: Yesterday Dow Jones change
dow_lastweek_changeP: Last Week Dow Jones change
nasdaq_yes_changeP: Yesterday NASDAQ 100 change
nasdaq_lastweek_changeP: Last Week NASDAQ 100 change
today_changeP: Today Amazon's stock price change
To learn more about the dataset and see a very simple prediction model applied to the dataset you may watch this YouTube Video where I have explained the dataset and also prediction: A Taste for Prediction: Predict Tomorrow's Amazon Stock Price

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43741) of an [OpenML dataset](https://www.openml.org/d/43741). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43741/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43741/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43741/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

